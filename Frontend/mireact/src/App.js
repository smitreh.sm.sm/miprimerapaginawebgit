import React, { Fragment, useState, useEffect } from 'react';
import Cita from './components/Pedido';
import Formulario from './components/Formulario';

function App() {
  let iniciaPedidos = JSON.parse(localStorage.getItem('pedidos1'));
  if(!iniciaPedidos){
    iniciaPedidos = [];
  }

  // ARREGLO DE CITAS
  const [LosPedidos, savePedidos] = useState(iniciaPedidos)

  // USEEFECT PARA REALIZAR CIERTAS OPERACIONES CUANDO EL STATE CAMBIA
  useEffect(() => {
    localStorage.setItem('pedidos1', JSON.stringify(LosPedidos));
  }, [LosPedidos])

  // FUNCION QUE TOME LAS CITAS ACTUALES Y AGREGUE LA NUEVA
  const crearPedidos1 = ElpedidoNuevo => {
    savePedidos([
      ...LosPedidos,
      ElpedidoNuevo
    ])
  }

  // FUNCION ELIMINAR CITA
  const eliminarCita = (id) => {
    const nuevasCitas = LosPedidos.filter(pedido => pedido.id !== id);
    savePedidos(nuevasCitas);
  }

  return (
    <Fragment>
      <h1>Pedidos hamburgueseria</h1>
      <div className="container mx-auto">
        <div className="grid grid-cols-1 md:grid-cols-3">
          <div>
            <Formulario
              crearPedidos1={crearPedidos1}
            />
          </div>
          <div>
            <h2>Mis Pedidos</h2>
            {LosPedidos.map(pedido => (
             <Cita
              key={pedido.id}
              pedido={pedido}
              eliminarCita={eliminarCita}
             />
            ))}
          </div>
          <div>
            <table border="1px">
            <h2>Lista de productos</h2>
            <tr>
            <td><b>Hamburguesa 7 Bs</b></td>
            <td><b><img src="https://www.hogar.mapfre.es/media/2018/09/hamburguesa-sencilla.jpg"></img></b></td>
            </tr>
            <tr>
            <td><b>Papas 5 Bs</b></td>
            <td><img src="https://i.blogs.es/899e53/650_1200/1366_2000.jpg" ></img></td>
            </tr>
            <tr>
            <td><b>Salchipapa 7 Bs</b></td>
            <td><img src= "https://www.laylita.com/recetas/wp-content/uploads/Salchipapas-con-salsa-rosado-y-curtido-de-cebolla.jpg">
                  </img></td>
            </tr>
            <tr>
            <td><b>Queso con papas 9 Bs</b></td>
            <td><img src="https://www.hellmanns.com/content/dam/unilever/global/recipe_image/125/12544/125449-default.jpg/_jcr_content/renditions/cq5dam.web.1072.804.jpeg">
                  </img></td>
            </tr>
            <tr>
            <td><b>Hamburguesa con papas 10 Bs</b></td>
            <td> <img src="https://st.depositphotos.com/1419868/1253/i/950/depositphotos_12532164-stock-photo-burger-and-french-fries.jpg">
                </img></td>
            </tr>
            <tr>
            <td><b>Refresco personal 2 Bs</b></td>
            <td> <img src="http://cafesybares.com/wp-content/uploads/2015/06/detail_3nPmy9VYTGe5DAFabcHj6NtgJ78QUx.jpg">
                  </img></td>
            </tr>
            </table>   
             </div>
             
            
        </div>
      </div>
    </Fragment>
  );
}

export default App;
