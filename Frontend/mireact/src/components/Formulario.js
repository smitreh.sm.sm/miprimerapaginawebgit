import React, { Fragment, useState } from 'react'
import { v4 as uuid } from 'uuid';

const Formulario = ({crearPedidos1}) => {
  
  const [pedido, actualizarPedidos] = useState({
    nombrecliente: '',
    clientepedido: '',
    fecha: '',
    hora: '',
    comentarios: 'gracias',
  })

  const [error, actualizarError] = useState(false)

  // FUNCION QUE SE EJECUTA CADA QUE EL USUARIO ESCRIBE
  const actualizarState = e => {
    actualizarPedidos({
      ...pedido,
      [e.target.name]: e.target.value
    })
  }

  // CUANDO EL USUARIO PRESIONE BOTON
  const submitcita = e =>{
    e.preventDefault();
    if(nombrecliente.trim() === '' || clientepedido.trim() === '' || fecha.trim() === '' || hora.trim() === ''){
      actualizarError(true);
      return;
    }
    actualizarError(false)
    
    // ASIGNAR ID 
    pedido.id = uuid()

    // CREAR pedido
    crearPedidos1(pedido)

    // REINICIAR FORM
    actualizarPedidos({
      nombrecliente: '',
      clientepedido: '',
      fecha: '',
      hora: '',
      comentarios: 'gracias',
    })
  }

  // EXTRAER LOS VALOR
  const {nombrecliente, clientepedido, fecha, hora, comentarios} = pedido

  return (
    <Fragment>
      <h2>Crear Pedido</h2>

      {error ? <p className="bg-yellow-400 px-2 py-4 text-white rounded-lg text-center">Todos los campos son obligatorios</p> : null}

      <form onSubmit={submitcita}>
        <label className="text-white">Nombre Cliente</label>
        <input
          type="text"
          name="nombrecliente"
          className="w-full border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent rounded-lg"
          placeholder="Nombre completo"
          onChange={actualizarState}
          value={nombrecliente}
        />

        <label className="text-white">Pedido</label>
        <input
          type="text"
          name="clientepedido"
          className="w-full border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent rounded-lg"
          placeholder="Pedido"
          onChange={actualizarState}
          value={clientepedido}
        />

        <label className="text-white">Fecha</label>
        <input
          type="date"
          name="fecha"
          className="w-full border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent rounded-lg p-2"
          onChange={actualizarState}
          value={fecha}
        />

        <label className="text-white">Hora</label>
        <input
          type="time"
          name="hora"
          className="w-full border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent rounded-lg p-2"
          onChange={actualizarState}
          value={hora}
        />

        <label className="text-white">Comentarios</label>
        <textarea
          name="comentarios"
          className="w-full border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent rounded-lg"
          onChange={actualizarState}
          value={comentarios}
        ></textarea>

        <button
          type="submit"
          className="w-full bg-white text-yellow-500 border-yellow-500 hover:bg-yellow-400 text-white px-4 rounder-lg shadow-md font-semibold"
        >Agregar Pedido
        </button>
      </form>
    </Fragment>
  )
}

export default Formulario
