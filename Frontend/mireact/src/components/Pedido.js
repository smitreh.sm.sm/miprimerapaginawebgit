import React from 'react'

const Pedido = ({pedido, eliminarPedido}) => {
  return (
    <div className="p-5  md:w-96 bg-gradient-to-r from-red-300 to-blue-300 border-gray-100 mb-4 ml-12 rounded-md">
      <p className="font-bold mb-3">Cliente <span className="font-normal">{pedido.nombrecliente}</span></p>
      <p className="font-bold mb-3">Pedido <span className="font-normal">{pedido.clientepedido}</span></p>
      <p className="font-bold mb-3">Fecha <span className="font-normal">{pedido.fecha}</span></p>
      <p className="font-bold mb-3">Hora <span className="font-normal">{pedido.hora}</span></p>
      <p className="font-bold mb-3">Comentarios <span className="font-normal">{pedido.comentarios}</span></p>

      <button 
        className="w-full bg-white text-yellow-500 border-yellow-500 hover:bg-yellow-300 hover:text-white"
        onClick={() => eliminarPedido(pedido.id)}
      >Eliminar</button>
    </div>
  )
}

export default Pedido
